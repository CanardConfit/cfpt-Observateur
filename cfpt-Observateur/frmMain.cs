﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace cfpt_Observateur
{
    public partial class frmMain : Form
    {
        private List<Voiture> _list;

        private Feux _feux;
        
        public frmMain()
        {
            InitializeComponent();

            _feux = new Feux();

            _list = new List<Voiture>
            {
                new("Tesla", false), 
                new("Lexus", true), 
                new("Volvo", true)
            };

            foreach (Voiture voiture in _list)
                if (voiture.Observe)
                    _feux.AddObserver(voiture.ObserveEtatFeux);

            Update();
        }

        private void Update()
        {
            pnlFeuVert.BackColor = _feux.EtatFeux == Feux.Etat.Vert ? Color.Green : Color.White;
            pnlFeuOrange.BackColor = _feux.EtatFeux == Feux.Etat.Orange ? Color.Orange : Color.White;
            pnlFeuRouge.BackColor = _feux.EtatFeux == Feux.Etat.Rouge ? Color.Red : Color.White;
            
            for (int i = 1; i <= _list.Count; i++)
            {
                Voiture v = _list[i - 1];
                ((CheckBox) Controls.Find($"cbxV{i}", false)[0]).Checked = v.Observe;
                ((Label) gbxVoitures.Controls.Find($"lblVoiture{i}", false)[0]).Text = v.GetText();
            }
        }

        private void Elements_Click(object sender, EventArgs e)
        {
            if (sender is Button btn)
            {
                switch (btn.Name)
                {
                    case "btnChangeEtat":
                        _feux.NextEtatFeux();
                        Update();
                        break;
                }
            }
            else if (sender is CheckBox cbx)
            {
                int id = Int32.Parse(cbx.Name.Replace("cbxV", ""))-1;
                Voiture v = _list[id];
                
                v.Observe = cbx.Checked;
                if (cbx.Checked)
                    _feux.AddObserver(v.ObserveEtatFeux);
                else
                    _feux.DelObserver(v.ObserveEtatFeux);
                
                Update();
            }
        }
    }
}