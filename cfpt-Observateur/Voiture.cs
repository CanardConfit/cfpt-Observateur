﻿namespace cfpt_Observateur
{
    public class Voiture
    {
        public enum Etat {Mouvement, Arretée}
        
        private string _marque;
        private bool _observe;
        private Etat _etatVoiture = Etat.Arretée;

        public Etat EtatVoiture
        {
            get => _etatVoiture;
            set => _etatVoiture = value;
        }

        public string Marque
        {
            get => _marque;
            set => _marque = value;
        }

        public bool Observe
        {
            get => _observe;
            set => _observe = value;
        }

        public Voiture(string marque, bool observe)
        {
            _marque = marque;
            _observe = observe;
        }

        public string GetText()
        {
            switch (_etatVoiture)
            {
                case Etat.Arretée:
                    return $"La {_marque} est à l'arrêt !";
                case Etat.Mouvement:
                    return $"La {_marque} est en mouvement !";
            }

            return null;
        }

        public void ObserveEtatFeux(Feux feu)
        {
            switch (feu.EtatFeux)
            {
                case Feux.Etat.Orange:
                case Feux.Etat.Rouge:
                    _etatVoiture = Etat.Arretée;
                    break;
                case Feux.Etat.Vert:
                    _etatVoiture = Etat.Mouvement;
                    break;
            }
        }
    }
}