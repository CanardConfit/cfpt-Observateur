﻿namespace cfpt_Observateur
{
    partial class frmMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblVoitureAffichage1 = new System.Windows.Forms.Label();
            this.gbxVoitures = new System.Windows.Forms.GroupBox();
            this.lblVoiture3 = new System.Windows.Forms.Label();
            this.lblVoitureAffichage3 = new System.Windows.Forms.Label();
            this.lblVoiture2 = new System.Windows.Forms.Label();
            this.lblVoitureAffichage2 = new System.Windows.Forms.Label();
            this.lblVoiture1 = new System.Windows.Forms.Label();
            this.cbxV1 = new System.Windows.Forms.CheckBox();
            this.btnChangeEtat = new System.Windows.Forms.Button();
            this.pnlFeuRouge = new System.Windows.Forms.Panel();
            this.gbxFeux = new System.Windows.Forms.GroupBox();
            this.pnlFeux = new System.Windows.Forms.Panel();
            this.pnlFeuVert = new System.Windows.Forms.Panel();
            this.pnlFeuOrange = new System.Windows.Forms.Panel();
            this.cbxV2 = new System.Windows.Forms.CheckBox();
            this.cbxV3 = new System.Windows.Forms.CheckBox();
            this.gbxVoitures.SuspendLayout();
            this.gbxFeux.SuspendLayout();
            this.pnlFeux.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblVoitureAffichage1
            // 
            this.lblVoitureAffichage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblVoitureAffichage1.Location = new System.Drawing.Point(29, 39);
            this.lblVoitureAffichage1.Name = "lblVoitureAffichage1";
            this.lblVoitureAffichage1.Size = new System.Drawing.Size(100, 23);
            this.lblVoitureAffichage1.TabIndex = 0;
            this.lblVoitureAffichage1.Text = "Voiture 1";
            // 
            // gbxVoitures
            // 
            this.gbxVoitures.Controls.Add(this.lblVoiture3);
            this.gbxVoitures.Controls.Add(this.lblVoitureAffichage3);
            this.gbxVoitures.Controls.Add(this.lblVoiture2);
            this.gbxVoitures.Controls.Add(this.lblVoitureAffichage2);
            this.gbxVoitures.Controls.Add(this.lblVoiture1);
            this.gbxVoitures.Controls.Add(this.lblVoitureAffichage1);
            this.gbxVoitures.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.gbxVoitures.Location = new System.Drawing.Point(12, 12);
            this.gbxVoitures.Name = "gbxVoitures";
            this.gbxVoitures.Size = new System.Drawing.Size(785, 561);
            this.gbxVoitures.TabIndex = 1;
            this.gbxVoitures.TabStop = false;
            this.gbxVoitures.Text = "Voitures (Observateurs)";
            // 
            // lblVoiture3
            // 
            this.lblVoiture3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblVoiture3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblVoiture3.Location = new System.Drawing.Point(29, 380);
            this.lblVoiture3.Name = "lblVoiture3";
            this.lblVoiture3.Size = new System.Drawing.Size(723, 117);
            this.lblVoiture3.TabIndex = 8;
            this.lblVoiture3.Text = "lblVoiture3";
            this.lblVoiture3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblVoitureAffichage3
            // 
            this.lblVoitureAffichage3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblVoitureAffichage3.Location = new System.Drawing.Point(29, 354);
            this.lblVoitureAffichage3.Name = "lblVoitureAffichage3";
            this.lblVoitureAffichage3.Size = new System.Drawing.Size(100, 23);
            this.lblVoitureAffichage3.TabIndex = 7;
            this.lblVoitureAffichage3.Text = "Voiture 3";
            // 
            // lblVoiture2
            // 
            this.lblVoiture2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblVoiture2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblVoiture2.Location = new System.Drawing.Point(29, 220);
            this.lblVoiture2.Name = "lblVoiture2";
            this.lblVoiture2.Size = new System.Drawing.Size(723, 117);
            this.lblVoiture2.TabIndex = 6;
            this.lblVoiture2.Text = "lblVoiture2";
            this.lblVoiture2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblVoitureAffichage2
            // 
            this.lblVoitureAffichage2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblVoitureAffichage2.Location = new System.Drawing.Point(29, 194);
            this.lblVoitureAffichage2.Name = "lblVoitureAffichage2";
            this.lblVoitureAffichage2.Size = new System.Drawing.Size(100, 23);
            this.lblVoitureAffichage2.TabIndex = 5;
            this.lblVoitureAffichage2.Text = "Voiture 2";
            // 
            // lblVoiture1
            // 
            this.lblVoiture1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblVoiture1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblVoiture1.Location = new System.Drawing.Point(29, 65);
            this.lblVoiture1.Name = "lblVoiture1";
            this.lblVoiture1.Size = new System.Drawing.Size(723, 117);
            this.lblVoiture1.TabIndex = 4;
            this.lblVoiture1.Text = "lblVoiture1";
            this.lblVoiture1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbxV1
            // 
            this.cbxV1.Location = new System.Drawing.Point(313, 603);
            this.cbxV1.Name = "cbxV1";
            this.cbxV1.Size = new System.Drawing.Size(160, 24);
            this.cbxV1.TabIndex = 2;
            this.cbxV1.Text = "V1 : Observe le feu !";
            this.cbxV1.UseVisualStyleBackColor = true;
            this.cbxV1.Click += new System.EventHandler(this.Elements_Click);
            // 
            // btnChangeEtat
            // 
            this.btnChangeEtat.Location = new System.Drawing.Point(856, 579);
            this.btnChangeEtat.Name = "btnChangeEtat";
            this.btnChangeEtat.Size = new System.Drawing.Size(176, 71);
            this.btnChangeEtat.TabIndex = 3;
            this.btnChangeEtat.Text = "Changer d\'état";
            this.btnChangeEtat.UseVisualStyleBackColor = true;
            this.btnChangeEtat.Click += new System.EventHandler(this.Elements_Click);
            // 
            // pnlFeuRouge
            // 
            this.pnlFeuRouge.BackColor = System.Drawing.Color.White;
            this.pnlFeuRouge.Location = new System.Drawing.Point(12, 16);
            this.pnlFeuRouge.Name = "pnlFeuRouge";
            this.pnlFeuRouge.Size = new System.Drawing.Size(150, 150);
            this.pnlFeuRouge.TabIndex = 4;
            // 
            // gbxFeux
            // 
            this.gbxFeux.Controls.Add(this.pnlFeux);
            this.gbxFeux.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.gbxFeux.Location = new System.Drawing.Point(819, 12);
            this.gbxFeux.Name = "gbxFeux";
            this.gbxFeux.Size = new System.Drawing.Size(240, 561);
            this.gbxFeux.TabIndex = 2;
            this.gbxFeux.TabStop = false;
            this.gbxFeux.Text = "Feux de circulation (Sujet)";
            // 
            // pnlFeux
            // 
            this.pnlFeux.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlFeux.Controls.Add(this.pnlFeuVert);
            this.pnlFeux.Controls.Add(this.pnlFeuOrange);
            this.pnlFeux.Controls.Add(this.pnlFeuRouge);
            this.pnlFeux.Location = new System.Drawing.Point(37, 39);
            this.pnlFeux.Name = "pnlFeux";
            this.pnlFeux.Size = new System.Drawing.Size(176, 494);
            this.pnlFeux.TabIndex = 5;
            // 
            // pnlFeuVert
            // 
            this.pnlFeuVert.BackColor = System.Drawing.Color.White;
            this.pnlFeuVert.Location = new System.Drawing.Point(12, 328);
            this.pnlFeuVert.Name = "pnlFeuVert";
            this.pnlFeuVert.Size = new System.Drawing.Size(150, 150);
            this.pnlFeuVert.TabIndex = 5;
            // 
            // pnlFeuOrange
            // 
            this.pnlFeuOrange.BackColor = System.Drawing.Color.White;
            this.pnlFeuOrange.Location = new System.Drawing.Point(12, 172);
            this.pnlFeuOrange.Name = "pnlFeuOrange";
            this.pnlFeuOrange.Size = new System.Drawing.Size(150, 150);
            this.pnlFeuOrange.TabIndex = 5;
            // 
            // cbxV2
            // 
            this.cbxV2.Location = new System.Drawing.Point(479, 603);
            this.cbxV2.Name = "cbxV2";
            this.cbxV2.Size = new System.Drawing.Size(156, 24);
            this.cbxV2.TabIndex = 4;
            this.cbxV2.Text = "V2 : Observe le feu !";
            this.cbxV2.UseVisualStyleBackColor = true;
            this.cbxV2.Click += new System.EventHandler(this.Elements_Click);
            // 
            // cbxV3
            // 
            this.cbxV3.Location = new System.Drawing.Point(641, 603);
            this.cbxV3.Name = "cbxV3";
            this.cbxV3.Size = new System.Drawing.Size(156, 24);
            this.cbxV3.TabIndex = 5;
            this.cbxV3.Text = "V3 : Observe le feu !";
            this.cbxV3.UseVisualStyleBackColor = true;
            this.cbxV3.Click += new System.EventHandler(this.Elements_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1078, 662);
            this.Controls.Add(this.cbxV3);
            this.Controls.Add(this.cbxV2);
            this.Controls.Add(this.gbxFeux);
            this.Controls.Add(this.btnChangeEtat);
            this.Controls.Add(this.cbxV1);
            this.Controls.Add(this.gbxVoitures);
            this.Name = "frmMain";
            this.Text = "Feux de circulation et voiture (Observateurs - Sujet)";
            this.gbxVoitures.ResumeLayout(false);
            this.gbxFeux.ResumeLayout(false);
            this.pnlFeux.ResumeLayout(false);
            this.ResumeLayout(false);
        }

        private System.Windows.Forms.CheckBox cbxV2;
        private System.Windows.Forms.CheckBox cbxV3;

        private System.Windows.Forms.Label lblVoiture2;
        private System.Windows.Forms.Label lblVoitureAffichage2;
        private System.Windows.Forms.Label lblVoiture3;
        private System.Windows.Forms.Label lblVoitureAffichage3;

        private System.Windows.Forms.Label lblVoiture1;

        private System.Windows.Forms.Panel pnlFeuOrange;
        private System.Windows.Forms.Panel pnlFeuVert;

        private System.Windows.Forms.Panel pnlFeux;

        private System.Windows.Forms.Button btnChangeEtat;
        private System.Windows.Forms.CheckBox cbxV1;
        private System.Windows.Forms.GroupBox gbxVoitures;
        private System.Windows.Forms.GroupBox gbxFeux;
        private System.Windows.Forms.Label lblVoitureAffichage1;
        private System.Windows.Forms.Panel pnlFeuRouge;

        #endregion
    }
}