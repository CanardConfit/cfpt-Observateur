﻿namespace cfpt_Observateur
{
    public class FeuxEventArgs
    {
        private Feux _feux;

        public Feux Feux => _feux;

        public FeuxEventArgs(Feux feux)
        {
            _feux = feux;
        }
    }
}