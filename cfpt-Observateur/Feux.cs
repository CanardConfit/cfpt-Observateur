﻿using System;
using System.Collections.Generic;

namespace cfpt_Observateur
{
    public class Feux
    {
        public delegate void EtatChange(FeuxEventArgs args);
        
        public enum Etat {Rouge = 0, Orange = 1, Vert = 2}

        private Etat _etatFeux = Etat.Rouge;
        private bool _etatFeuxInvert = false;

        private List<Action<Feux>> _observers;
        
        public Etat EtatFeux
        {
            get => _etatFeux;
            set
            {
                _etatFeux = value;
                //TODO Fire event EtatChange.
            }
        }

        public void NextEtatFeux()
        {
            if (_etatFeux == Etat.Vert)
                _etatFeuxInvert = true;
            else if (_etatFeux == Etat.Rouge)
                _etatFeuxInvert = false;
            
            EtatFeux = _etatFeux + (_etatFeuxInvert ? -1 : 1);
        }

        public Feux()
        {
            _observers = new List<Action<Feux>>();
        }

        public void AddObserver(Action<Feux> action)
        {
            _observers.Add(action);
        }

        public void DelObserver(Action<Feux> action)
        {
            _observers.Remove(action);
        }
    }
}